import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  //selectedMovie ile alakalı değişken tanımlıyoruz
  //bu item bize dışarıdan geliyor bunun icin
  //buraya input decorator kullanmalıyız
  //movies.component de şunu yapmalıyız
  //[movie]="selectedMovie"
  //işte bunu input decorator deniyor
  @Input() movie: Movie;
  constructor(
    private movieService: MovieService,
    //id bilgisine ulaşabilmek icin route bilgisine ihtiyacımız var
    private route: ActivatedRoute,
    private location: Location
  ) { }

  //localhost:4200/detail/2
  //id =2 bunu almalıyız
  //bunun icin servise ihtiyacımız var bunu da kurucu fonk icinde tanımlayacagız



  ngOnInit() {
    this.getMovie();
  }

  getMovie(): void {
    //route üzerindeki id bilgisini bu şekilde alabiliyoruz
    //yani /localhost:4200/detail/2 buradaki 2 yi
    //+ eklememizin sebebi gelen bilgi string idi
    //onu numbera çevirmek icin böyle yaptık
    const id = +this.route.snapshot.paramMap.get('id');
    //getMovie Observable old icin gelen bilgiyi
    //subscribe ile almamız lazım
    this.movieService.getMovie(id)
      .subscribe(movie => this.movie = movie);
  }



  //http üzerinde update yapmak icin yazıldı
  //location tarayıcıda bir önceki sayfaya gitmek icin yapıldı
  save(): void {
    this.movieService.update(this.movie)
      .subscribe(() => {
        this.location.back();
        //bir önceki sayfaya gitmek icin

      });
  }
}
