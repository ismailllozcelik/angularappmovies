import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';


const routes:Routes=[
  //path:'', redirectTo:'/dashboard',pathMatch:'full'
  //bunun anlamı path boş gelirse aşağıdakine gönder demek
  //bunun yerine componenti aşağıdaki gibi yapabiliriz
  {  path:'', redirectTo:'/dashboard',pathMatch:'full'},
  {  path:'dashboard',component:DashboardComponent },
  {  path:'movies',component:MoviesComponent },
  //:id diyerekten orada değişken tanımlaması yapmış oluyoruz
  {  path:'detail/:id',component:MovieDetailComponent }
];
@NgModule({

  //bu modülü app module kullanacağı için export etmemiz lazım
  exports:[RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
