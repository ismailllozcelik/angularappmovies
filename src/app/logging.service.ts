import { Injectable } from '@angular/core';

//servisin ulaşabilirligi root old icin
//her yerden ulaşabiliriz
@Injectable({
  providedIn: 'root'
})
export class LoggingService {
//bunları http üzerinden kayıt etseydik o zaman
//observable ile degistirmemiz gerekirdi
  messages: string[] = [];

add(message:string)
{
  this.messages.push(message);
}
clear(){
  this.messages=[];
}

  constructor() { }
}
