import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {

    const movies = [

      { id: 1, name: "movie 1", description: "Güzel bir film", imageUrl: "1.webp" },
      { id: 2, name: "movie 2", description: "Fena Değil", imageUrl: "2.webp" },
      { id: 3, name: "movie 3", description: "eee işte", imageUrl: "3.jpg" },
      { id: 4, name: "movie 4", description: "izleme kaç", imageUrl: "4.webp" },
      { id: 5, name: "movie 5", description: "harika bir film", imageUrl: "5.jpg" },
      { id: 6, name: "movie 6", description: "efso", imageUrl: "6.webp" }

    ];
    return { movies };
  }
  constructor() { }
}
