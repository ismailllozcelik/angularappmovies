import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';
@Component({
  selector: 'movies',
  templateUrl: 'movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  title = 'Movie List';


  ngOnInit() {
    //bu fonksiyon contructordan hemen sonra çalışıyor
    this.getMovies();
  }
  movies: Movie[];
  selectedMovie: Movie;

  constructor(private movieService: MovieService) { }
  onSelect(movie: Movie): void {

    this.selectedMovie = movie;
  }

  getMovies(): void {
    //işte burada servisin önemi cıkıyor
    //servis vasıtasıyla ben bütün filmleri getirdim
    //yoksa her seferinde bu fonksiyoru her yerde yazmam gerekirdi
    //böylelikle bir kere yazdım ve her şey tamam



    //normalde böyle alıyorduk ama rxjs yi yani observable ı kullandığımız icin
    //burada şöyle bir değişiklik yapmalıyız
    //callback fonksiyonu ile this.movies in icini doldurduk
    this.movieService.getMovies()
      .subscribe(movies => {
        this.movies = movies;
      });

      //shift + alt +f e basınca kodlar düzeltilir görüntüsel olarak
  }
  //angularda html icinde döngü oluşturmak icin
  //ngFor kullan buna directive diyorlar
  //aşağıda object oluşturdum ve tipini de as Movie diyerek
  //movie old belirttim
add(name:string,imageUrl:string,description:string):void{
this.movieService.add({
  name,
  imageUrl,
  description
}as Movie).subscribe(movie=>this.movies.push(movie));
}

//api üzerinden delete işlemi icin

delete(movie:Movie):void{
  //ilk önce komponent üzerinden silelim hızlı görünsün
  //daha sonra servis üzerinden silelim

  //tıklanın haricindeki herşeyi yine movies nesnesine atıyor
  this.movies=this.movies.filter(m=>m!==movie);

  //şimdide servis üzerinden silelim
  this.movieService.delete(movie).subscribe();
}
}
