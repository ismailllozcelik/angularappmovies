import { Injectable } from '@angular/core';
import { Movie } from './movie';
import { Movies } from './movie.datasource';
import {Observable,of} from 'rxjs';
import { LoggingService } from './logging.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//bunu yazdıktan sonra artıkk get gibi istekleri
//http üzerinden yapabiliyoruz

@Injectable({
  //burdaki root un anlamı başka katmanlarda olabilir
  //ama bu service bu yazılımın en temelince çalışacağı icin böyle
  providedIn: 'root'
})
export class MovieService {


  //api adresi tanımlıyoruz
  private apiMoviesUrl='api/movies';
  //http yi kullanmamız icin inject etmemiz lazım
  constructor(
    private loggingService:LoggingService,
    private http:HttpClient
    ) {
   }

  //normalde nodejs ile veri çekerken bunu asekron bir
  //bicimde yapıyoruz. bilginin kac sn sonra geleceği belli değil,
  //hem diğer taraftan kodlar devam ediyor
  //bunu durdurmak icin Observable oluşturmak
  //normalde Movie[] dizisi dönecegimizi söylüyorduk
  //şimdi Observable ile çevreledik böyle dön diye asycn
  getMovies():Observable<Movie[]>{
    
    this.loggingService.add('MovieService : Listing movies');
    //http get böyle cagırılıyor
  return this.http.get<Movie[]>(this.apiMoviesUrl);
  }

  getMovie(id):Observable<Movie>{
    this.loggingService.add('MovieService: get detail by id='+id);
    //return of(Movies.find(movie=>movie.id===id));
    return this.http.get<Movie>(this.apiMoviesUrl+'/'+id);
  }

  update(movie:Movie):Observable<any>{
    //göndereceğim bilgiyi sunucuya açıklamak icin
    const httpOptions={
      headers:new HttpHeaders({'Content-Type':'application/json'})

    }
    return this.http.put(this.apiMoviesUrl,movie,httpOptions);
  }

  //burada farklı kriterlere göre id ye veya başka bir şeye
  //göre istenilen şeylerden alırız

  add(movie:Movie):Observable<Movie>{
    //post requestin icine yani body e movie bilgisini gönderiyoruz
    return this.http.post<Movie>(this.apiMoviesUrl,movie);
  }

  //şimdide servis üzerinden db deki verileri siliyoruz
  delete(movie:Movie):Observable<Movie>{
    return this.http.delete<Movie>(this.apiMoviesUrl+'/'+movie.id);
  }
}
